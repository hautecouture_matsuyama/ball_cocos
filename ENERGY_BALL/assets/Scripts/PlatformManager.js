
cc.Class({
    extends: cc.Component,

    properties: {
        colorTag: 0,    //エディタ上にて設定。0:赤、1:緑、2:紫、3:黄
    },

    // onLoad () {},

    getColorTag() {
        return this.colorTag;
    },

    //プレイヤーと接触時に色が一緒なら呼ばれる。
    clearCollider() {
        //親に自分含めた4つのコライダーを一時的に無効化してもらう。
        var parent = this.node.parent.getComponent('ScrollObject');
        parent.setActiveChildren(false);
    },

    start () {

    },

    // update (dt) {},
});
