
var UserData = {
    bestScore: 0,
    lastScore: 0,
};


cc.Class({
    extends: cc.Component,

    properties: {

        nowScore: cc.Label,

        bestLabel: cc.Label,
        lastLabel: cc.Label,

        _userData: null,

        now: 0,

        speedCount: 0,

    },

    // onLoad () {},

    start() {
        this.getScore();
        //自分の順位と最終スコア、ベストスコアを適用する。
        this.bestLabel.string = 'BEST\n' + this._userData.bestScore;
        this.lastLabel.string = 'LAST\n' + this._userData.lastScore;

        this.speedCount = 5;
    },

    countPlus() {
        this.now++;
        this.nowScore.string = this.now;

        this.speedCount--;
        //speedCountが0になったら乱数でスピードアップ(ダウン)カウントを再指定。
        if (this.speedCount <= 0) {
            // this.speedCount = Math.floor(Math.random() * (8 - 3) + 3);
            this.speedCount = 5;
            var game = cc.find('Canvas/GameManager').getComponent('GameManager');
            game.allSpeedChange();
        }
    },

    getScore() {
        var data = cc.sys.localStorage.getItem('UserData');
        cc.log(data);

        if (data != null) {
            this._userData = JSON.parse(data);
        }
        else {
            this._userData = UserData;
        }
    },

    setScore() {
        if (this._userData.bestScore < this.now) {
            this._userData.bestScore = this.now;

            var leaderBoard = cc.find('Canvas/leader_bord').getComponent('Leaderboard');
            leaderBoard.sendScore(this.now);
        }
        
        this._userData.lastScore = this.now;

        cc.sys.localStorage.setItem('UserData', JSON.stringify(this._userData));
    },

    // update (dt) {},
});
