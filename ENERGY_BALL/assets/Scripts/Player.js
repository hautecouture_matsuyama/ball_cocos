
var SoundManager = require('SoundManager');
var ScoreManager = require('ScoreManager');
var SpawnSystem = require('SpawnSystem');

cc.Class({
    extends: cc.Component,

    properties: {

        sound: SoundManager,
        score: ScoreManager,
        spawn: SpawnSystem,

        body: cc.RigidBody,
        colorTag: 0,    //0:赤、1:緑、2:紫、3:黄

        colors: {
            default: [],
            type: cc.Node,
        },

        shield: cc.Node,

        isStart: false,
        isHit: false,

        isMuteki: false,
    },

    onLoad() {
        //各当たり判定を有効にする。
        cc.director.getPhysicsManager().enabled = true;
        cc.director.getCollisionManager().enabled = true;
    },

    start() {
    },

    jump() {
        this.body.linearVelocity = cc.p(0, 850);
        this.sound.playSE(0);
        this.spawn.spawnParticle(this.node.position, false);
    },

    colorChange() {
        this.colorTag = Math.floor(Math.random() * 4);
        for (var i = 0; i < this.colors.length; i++) {
            if (i == this.colorTag)
                this.colors[i].active = true;
            else
                this.colors[i].active = false;
        }
    },

    onCollisionEnter(other, self) {

        if (this.isHit)
            return;

        if (other.node.name == 'Caution') {
            this.damage(true);
            return;
        }

        var color = other.node.getComponent('PlatformManager');
        if (this.colorTag == color.getColorTag()) {
            this.sound.playSE(1);
            this.colorChange();
            this.score.countPlus();
            color.clearCollider();
        }
        else if (this.colorTag != color.getColorTag()) {
            this.damage(false);
        }
    },

    damage(dead) {
        if (this.isMuteki && !dead) {
            this.isMuteki = false;

            this.sound.playBreak();
            this.shield.active = false;
            return;
        }
        this.isHit = true;
        this.sound.playSE(2);
        var game = cc.find('Canvas/GameManager').getComponent('GameManager');
        game.gameOver();
        this.node.opacity = 0;
        this.spawn.spawnParticle(this.node.position, true);

        this.score.setScore();

    },

    setMuteki() {
        this.isMuteki = true;
        this.shield.active = true;
    },

    update(dt) {
        if (this.node.x > -220 && !this.isStart) {
            //垂直落下
            var linear = this.body.linearVelocity;
            this.body.linearVelocity = cc.p(0, linear.y);

            this.isStart = true;
            var game = cc.find('Canvas/GameManager').getComponent('GameManager');
            game.setTouchEvent();
        }
    },
});
