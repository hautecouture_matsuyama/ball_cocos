
cc.Class({
    extends: cc.Component,

    properties: {

        colors: {
            default: [],
            type: cc.Node,
        },
        
    },

    // onLoad () {},

    start () {

    },

    shuffle() {

        for (var i = 0; i < this.colors.length; i++){
            var w = Math.floor(Math.random() * this.colors.length);
            var tmp = this.colors[i].getPosition();
            this.colors[i].setPosition(this.colors[w].getPosition());
            this.colors[w].setPosition(tmp);
        }
    }

    // update (dt) {},
});
