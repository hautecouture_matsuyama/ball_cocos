
cc.Class({
    extends: cc.Component,

    properties: {

        speed: 100,
        scrollLine: 0,
        distance: 1000,

        child: {
            default: [],
            type: cc.BoxCollider,
        },

        isActive: false,

    },

    // onLoad () {},

    // start() {},

    update(dt) {

        this.node.x += this.speed * dt;

        if (this.node.name == 'Caution') {
            if (this.speed > 0 && this.node.x > this.scrollLine) {
                this.node.x += this.distance;
            }
            else if (this.speed < 0 && this.node.x < -this.scrollLine) {
                this.node.x += this.distance;
            }
        }

        if (this.node.name == 'Line') {
            var shuffle = this.node.getComponent('Shuffle');

            if (this.speed > 0 && this.node.x > this.scrollLine) { }
            else if (this.speed < 0 && this.node.x < -this.scrollLine) { }
            else
                return;

            this.node.x += this.distance;
            shuffle.shuffle();
            if (this.isActive == false)
                this.setActiveChildren(true);
        }
    },

    setActiveChildren(value) {
        for (var i = 0; i < this.child.length; i++) {
            this.child[i].enabled = value;
        }
        this.isActive = value;
    },

    speedChange(value) {
        this.speed += value;
    },

});