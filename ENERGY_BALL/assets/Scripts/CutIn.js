
cc.Class({
    extends: cc.Component,

    properties: {
        startPos: cc.Vec2,
        middlePos: cc.Vec2,
        endPos: cc.Vec2,
    },

    // onLoad () {},

    start () {

    },

    slideIn() {
        var In = cc.moveTo(0.5, this.middlePos).easing(cc.easeOut(1));
        var End = cc.moveTo(0.5, this.endPos).easing(cc.easeIn(1));
        var Return = cc.moveTo(0, this.startPos);

        this.node.runAction(cc.sequence(In, End, Return));
    },

    // update (dt) {},
});
