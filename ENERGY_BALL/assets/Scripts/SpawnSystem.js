
cc.Class({
    extends: cc.Component,

    properties: {
        particlePrefab: cc.Prefab,
        particleParent: cc.Node,

        explosion: cc.Prefab,
    },

    // onLoad () {},

    start () {

    },

    spawnParticle(pos, isExp) {
        var prefab = this.particlePrefab;
        if (isExp == true)
            prefab = this.explosion;
        
        var obj = cc.instantiate(prefab);
        obj.parent = this.particleParent;
        obj.position = pos;
    },

    // update (dt) {},
});
