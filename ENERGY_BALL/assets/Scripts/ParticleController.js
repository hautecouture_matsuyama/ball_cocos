
cc.Class({
    extends: cc.Component,

    properties: {
        particles: {
            default: [],
            type: cc.Node,
        },

    },

    // onLoad () {},

    start() {
        //表示する色2色をランダムで決める。
        var rand = Math.floor(Math.random() * 4);
        var rand2;
        do {
            rand2 = Math.floor(Math.random() * 4);
        } while (rand == rand2);

        this.particles[rand].active = true;
        this.particles[rand2].active = true;

        setTimeout(() => {
            if (this.node != null)
                this.node.destroy();
        }, 1600);
    },

    // update (dt) {},
});
