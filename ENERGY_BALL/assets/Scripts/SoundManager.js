
var Sound = cc.Class({
    name: 'Sound',
    properties: {
        url: cc.AudioClip,
    },
});

cc.Class({
    extends: cc.Component,

    properties: {
        sounds: {
            default: [],
            type: Sound,
        },

        bgm:cc.AudioSource,

        push:cc.AudioClip,
        close:cc.AudioClip,

        alarm: cc.AudioClip,
        break: cc.AudioClip,
    },

    // onLoad () {},

    start () {

    },

    gameStart() {
        this.bgm.play();
    },

    playSE(No) {
        cc.audioEngine.play(this.sounds[No].url);
    },

    playAlarm() {
        cc.audioEngine.play(this.alarm,false,0.7);
    },

    pushButton() {
        cc.audioEngine.play(this.push);
    },

    pushClose() {
        cc.audioEngine.play(this.close);
    },

    playBreak() {
        cc.audioEngine.play(this.break);
    },

    // update (dt) {},
});
