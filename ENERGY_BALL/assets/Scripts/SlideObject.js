
cc.Class({
    extends: cc.Component,

    properties: {
        startPos: cc.Vec2,
        endPos: cc.Vec2,
        duration: 1,
        isStart: false,
    },

    // onLoad () {},

    start () {

    },

    slide() {
        var pos = this.isStart ? this.startPos : this.endPos;
        if (this.isStart)
            this.duration /= 3;
        var move = cc.moveTo(this.duration, pos).easing(cc.easeOut(1));
        if (this.node.name == 'Title_Label' && !this.isStart) {
            var callback = cc.callFunc(this.call, this);
            this.node.runAction(cc.sequence(move, callback));
        }
        else {
            this.node.runAction(move);
        }
        this.isStart = true;
    },

    call() {
        var canvas = cc.find('Canvas').getComponent('CanvasManager');
        canvas.removeFilter();
    }

    // update (dt) {},
});
