
var Player = require('Player');
var CanvasManager = require('CanvasManager');
var SoundManager = require('SoundManager');
var ScrollObject = require('ScrollObject');
var CutIn = require('CutIn');

cc.Class({
    extends: cc.Component,

    properties: {

        scrollObject: {
            default: [],
            type: ScrollObject
        },

        player: {
            default: null,
            type: Player,
        },
        canvasManager: CanvasManager,
        sound: SoundManager,


        gameNode: cc.Node,
        flashNode: cc.Node,

        speedUp: CutIn,
        speedDown: CutIn,

        speedFlash: cc.Node,
        flashAnim: cc.Animation,

        isGameOver: false,

        speed: -500,     //オブジェクトの速さ。増減の際の比較に使用
        minSpeed: -400,
        maxSpeed: -600,
    },

    onLoad() {
    },

    setTouchEvent() {
        var self = this;
        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouch: true,

            onTouchBegan: function (touch, event) {
                self.setJump();
                return false;
            }
        }, self.node);
    },

    setJump() {
        if (!this.isGameOver) {
            this.player.jump();
        }
    },

    allSpeedChange() {
        var rate = Number;
        do {
            rate = Math.floor(Math.random() * 3) - 1;   //(-1~1)
        } while (rate == 0);

        //変化速度・・・ (-1 || 1)*(1~2)*50・・・-100,-50,50,100
        var value = rate * Math.floor(Math.random() * (3 - 1) + 1) * 50;

        //すでに限界値なら増減値を反転させる。
        if (this.speed >= this.minSpeed || this.speed <= this.maxSpeed)
            value *= -1;

        //増減値が猶予を超えるなら猶予分だけ変化させる。
        if (this.speed + value > this.minSpeed) {
            value = this.minSpeed - this.speed;
        }
        else if (this.speed + value < this.maxSpeed) {
            value = this.maxSpeed - this.speed;
        }

        cc.log('増減値：' + value);
        if (Math.floor(value) == 0) {
            this.allSpeedChange();
            cc.log('再抽選');
            return;
        }

        this.speed += value;
        cc.log('変更後のスピード：' + this.speed);
        cc.log('----------------------------------------');

        if (value < 0) {
            this.speedFlash.color = new cc.Color(249, 31, 136);
            this.speedUp.slideIn();
        }
        else {
            this.speedFlash.color = new cc.Color(25, 163, 251);
            this.speedDown.slideIn();
        }

        this.flashAnim.play();
        this.sound.playAlarm();

        for (var i = 0; i < this.scrollObject.length; i++) {
            this.scrollObject[i].speedChange(value);
        }
    },

    gameStart() {
        if (this.gameNode.active == false) {
            this.gameNode.active = true;
            this.canvasManager.allSlide();
            this.sound.gameStart();
            // console.log('ゲーム開始')
        }
        else {
            // console.log('ゲーム開始済み');
        }
    },

    gameOver() {
        cc.sys.localStorage.setItem('isDead', 1);
        this.isGameOver = true;
        this.flashNode.active = true;

        setTimeout(() => {
            cc.director.loadScene('Game');
        }, 1600);
    },

    start() {

    },

    // update (dt) {},
});
