var SlideObject = require('SlideObject');

cc.Class({
    extends: cc.Component,

    properties: {
        slideObject: {
            default: [],
            type: SlideObject,
        },

        leaderboard: cc.Node,

        HowTo: cc.Node,

        tuto1: cc.Node,
        tuto2: cc.Node,

        filter: cc.Node,
        adsBoard: cc.Node,

        //--------------------------------------------------------

        isShow: false,

        possibleInter: null,         //
        possibleVideo: null,         //表示可能かどうか(読み込み済かどうか)

        isRewardPanel: false,   //リワードパネルを表示させるかどうか。
        isInterStitial: false,  //インターを表示させるかどうか。

        //--------------------------------------------------------

        past: null,
        now: null,
    },

    // onLoad () {},

    start() {
        this.past = Date.now();

        //広告系はゲームオーバーを挟んだ後に処理する。
        var data = cc.sys.localStorage.getItem('isDead');
        if (data != null && data == 1 && typeof FBInstant != 'undefined') {
            cc.sys.localStorage.setItem('isDead', 0);

            //25%で動画広告パネル表示(コレを表示したらインタースティシャルは表示しない)
            var rand = Math.random() * 11;
            if (rand <= 2.5) {
                // console.log('動画広告表示');
                this.isRewardPanel = true;
                this.isShow = true;
            }
            //表示しないなら今度はインタースティシャルを確率で表示させる。
            else {
                var rand2 = Math.random() * 11;
                if (rand2 <= 2.5) {
                    // console.log('インタースティシャル表示');
                    this.isInterStitial = true;
                    this.isShow = true;
                }
                else {
                    // console.log('広告表示なし');
                }
            }
        }

        this.allSlide();
    },

    removeFilter() {
        if (this.isShow == false) {
            this.filter.active = false;
            // console.log('----------UI移動終了----------');
            // console.log('フィルター解除');
            // this.now = Date.now();
            // console.log('解除した時間：' + (this.now - this.past));
            // console.log('----------------------------------------')
        }
    },

    setPossible(_isInter, _isVideo) {
        //成功
        if (_isInter == null && _isVideo == true)
            this.possibleVideo = _isVideo;
        else if (_isInter == true && _isVideo == null) {
            this.possibleInter = _isInter;
            this.showAds();     //showAdsで条件に合っていればインタースティシャル表示。
        }
        //失敗
        else if (_isInter == null && _isVideo == false) {
            this.possibleInter = _isInter;

            //リワード表示フラグが立っていて、インタースティシャルもだめならフィルター解除
            if (this.isRewardPanel == true && this.possibleInter != null) {
                if (this.possibleInter == false) {
                    this.adFailed();
                    // console.log('動画広告を表示しようとしましたが失敗しました(後：動画)')

                }
            }
        }
        else if (_isInter == false && _isVideo == null) {
            this.possibleVideo = _isVideo;

            //リワード表示フラグが立っていて、動画もだめならフィルター解除。
            if (this.isRewardPanel == true && this.possibleVideo != null) {
                if (this.possibleVideo == false) {
                    this.adFailed();
                    // console.log('動画広告を表示しようとしましたが失敗しました(後：インター)');
                    return;
                }
                else {
                    return;     //動画広告が読み込めてるなら問題なし
                }
            }
            this.adFailed();
            // console.log('インタースティシャルを表示しようとしましたが失敗しました');

        }
    },

    adFailed() {
        this.isShow = false;
    },

    showAds() {
        if (this.possibleInter == false && this.possibleVideo == false) {
            return;
        }

        //表示フラグと読み込みフラグが立ってるなら表示させる。
        if (this.isRewardPanel) {
            //リワードはどちらかが読み込めれていれば構わない。
            if (this.possibleInter == true || this.possibleVideo == true) {
                this.adsBoard.active = true;
                this.filter.active = false;
            }
        }

        if (this.isInterStitial == true && this.possibleInter == true) {
            var inter = cc.find('Canvas/Ads').getComponent('AdInterstitial');
            inter.showAd(false);
            this.filter.active = false;
        }

    },

    allSlide() {
        for (var i = 0; i < this.slideObject.length; i++) {
            this.slideObject[i].slide();
        }
    },

    showRanking() {
        this.leaderboard.active = true;
    },

    showHowTo() {
        this.HowTo.active = true;
    },

    changePage() {
        this.tuto1.active = !this.tuto1.active;
        this.tuto2.active = !this.tuto2.active;
    },



    // update (dt) {},
});