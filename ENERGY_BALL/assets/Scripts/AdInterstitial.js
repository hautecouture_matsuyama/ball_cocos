
//  IDの直入力でないとエラーが発生するので注意。

var preloadedInterstitial = null;

cc.Class({
    extends: cc.Component,

    properties: {
    },

    onLoad() {
        this.initialize();
    },

    // start() {
    //     this.initialize();
    // },

    initialize() {
        var self = this;

        var canvas = cc.find('Canvas').getComponent('CanvasManager');

        //実行時(Chromeでの確認)にエラーが出ないようにする。
        if (typeof FBInstant == 'undefined') {
            return;
        }

        FBInstant.getInterstitialAdAsync(
            '390595285096654_390595865096596',
        ).then(function (interstitial) {
            self.preloadedInterstitial = interstitial;
            return self.preloadedInterstitial.loadAsync();
        }).then(function () {
            console.log('Interstitial：Success!');
            canvas.setPossible(true, null);
        }).catch(function (err) {
            console.log('Interstitial：Failed.');
            canvas.setPossible(false, null);
        });

        
    },

    showAd(isReward) {
        //実行時(Chromeでの確認)にエラーが出ないようにする。
        if (typeof FBInstant == 'undefined') {
            console.log('Display failed.');
            return;
        }

        var self = this;

        if (self.preloadedInterstitial == null || typeof self.preloadedInterstitial == 'undefined')
            return;

        self.preloadedInterstitial.showAsync()
            .then(function () {
                console.log('Success!');
                if (isReward == true) {
                    var player = cc.find('Canvas/_Game/Player').getComponent('Player');
                    player.setMuteki();
                }
            })
            .catch(function (e) {
                console.log(e.message);
            });
    }

    // update (dt) {},
});
